﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace App1
{
    class NamedPipes
    {
        public void GoServer()
        {
            var pipe = new NamedPipeServerStream("my_stream");
            var writer = new StreamWriter(pipe);
            var reader = new StreamReader(pipe);

            Console.WriteLine("Wating for connection");
            pipe.WaitForConnection();
            Console.WriteLine("Connection from client accepted");

            Console.WriteLine("Message recieved: " + reader.ReadLine());
            Console.Write("Type message: ");
            string message = Console.ReadLine();
            writer.WriteLine(message);
            writer.Flush();
        }
    }
}
/*
        using (var pipe = new NamedPipeServerStream("my_stream"))
        {
            using (var writer = new StreamWriter(pipe))
            {
                using (var reader = new StreamReader(pipe))
                {
                    Console.WriteLine("Wating for connection");
                    pipe.WaitForConnection();
                    Console.WriteLine("Connection from client accepted");
                    Console.WriteLine("Message recieved: " + reader.ReadLine());
                    Console.Write("Type message: ");
                    string message = Console.ReadLine();
                    writer.Write(message);
                    writer.Flush();
                }
            }
        }
*/