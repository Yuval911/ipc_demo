﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App2
{
    class NamedPipes
    {
        public void GoClient()
        {
            var pipe = new NamedPipeClientStream("my_stream");
            var writer = new StreamWriter(pipe);
            var reader = new StreamReader(pipe);

            Console.WriteLine("Connecting to the pie...");
            pipe.Connect();
            Console.WriteLine("Connected successfuly to the pipe");

            Console.Write("Type message: ");
            string message = Console.ReadLine();
            writer.WriteLine(message);
            writer.Flush();
            Console.WriteLine("Message recieved: " + reader.ReadLine());
            Console.ReadKey();
        }
    }
}
/*
        using (var pipe = new NamedPipeClientStream("my_stream"))
        {
            using (var writer = new StreamWriter(pipe))
            {
                using (var reader = new StreamReader(pipe))
                {
                    Console.WriteLine("Connecting to the pie...");
                    pipe.Connect();
                    Console.WriteLine("Connected successfuly to the pipe");
                    Console.Write("Type message: ");
                    string message = Console.ReadLine();
                    writer.Write(message);
                    writer.Flush();
                    Console.WriteLine(reader.ReadLine());
                }
            }
        }
*/