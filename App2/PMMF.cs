﻿using System;
using System.Collections.Generic;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace App2
{
    class PMMF
    {
        public void GoUser()
        {
            // Assumes another process has created the memory-mapped file.
            using (var mmf = MemoryMappedFile.OpenExisting("ImgA"))
            {
                using (var accessor = mmf.CreateViewAccessor(0, 50))
                {
                    char a;
                    for (long i = 0; i < 40; i++)
                    {
                        accessor.ReadChar(i);
                    }
                }
            }
        }
    }
}

